package com.farahani.elmira.backbaseassignment.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.farahani.elmira.backbaseassignment.Async.JsonReadingAsyncRequest;
import com.farahani.elmira.backbaseassignment.Fragments.MainFragment;
import com.farahani.elmira.backbaseassignment.Interfaces.OnAsyncRequestComplete;
import com.farahani.elmira.backbaseassignment.Objects.CityInfo;
import com.farahani.elmira.backbaseassignment.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnAsyncRequestComplete {

    MainFragment mainFragment;
    private static boolean DOUBLE_BACK_TO_EXIT_PRESSED_ONCE = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainFragment = new MainFragment();
        JsonReadingAsyncRequest request = new JsonReadingAsyncRequest(MainActivity.this);
        request.execute("");
    }

    private void InitializeMainFragment(Bundle bundle) {
        mainFragment.setArguments(bundle);
        (MainActivity.this).getSupportFragmentManager().beginTransaction()
                .add(R.id.frlFragmentsContainer, mainFragment).commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        mainFragment.search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        if (fragmentManager.findFragmentById(R.id.frlFragmentsContainer).equals(mainFragment)) {
        }
        if (DOUBLE_BACK_TO_EXIT_PRESSED_ONCE && fragmentManager.findFragmentById(R.id.frlFragmentsContainer).equals(mainFragment)) {
            exitAppMethod();
        } else if (!fragmentManager.findFragmentById(R.id.frlFragmentsContainer).equals(mainFragment)) {

            super.onBackPressed();
        }
        if (fragmentManager.findFragmentById(R.id.frlFragmentsContainer).equals(mainFragment)) {
            this.DOUBLE_BACK_TO_EXIT_PRESSED_ONCE = true;
            Toast.makeText(this, getResources().getString(R.string.exit_message), Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    DOUBLE_BACK_TO_EXIT_PRESSED_ONCE = false;
                }
            }, 2000);

        }
    }

    public void exitAppMethod() {

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void asyncResponse(ArrayList<CityInfo> response) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("list", response);
        InitializeMainFragment(bundle);
    }
}
