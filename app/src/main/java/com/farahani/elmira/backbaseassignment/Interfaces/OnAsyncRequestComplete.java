 	package com.farahani.elmira.backbaseassignment.Interfaces;

	import com.farahani.elmira.backbaseassignment.Objects.CityInfo;

	import java.util.ArrayList;

	public interface OnAsyncRequestComplete {
	
	public void asyncResponse(ArrayList<CityInfo> response);

}
