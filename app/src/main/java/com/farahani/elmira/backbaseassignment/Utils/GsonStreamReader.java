package com.farahani.elmira.backbaseassignment.Utils;

import android.app.Activity;
import android.util.Log;

import com.farahani.elmira.backbaseassignment.Objects.CityInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Elmira on 12/20/2017.
 */

public class GsonStreamReader {
    Activity activity;
    public GsonStreamReader(Activity activity){
        this.activity=activity;
    }
    public Set<CityInfo> getCities() {

        Set<CityInfo> citiesList = new HashSet<>();
        String json = readFromAsset(activity, "cities.json");
        Type listType = new TypeToken<HashSet<CityInfo>>() {}.getType();

        try {
            citiesList = new Gson().fromJson(json, listType);
        }
        catch (Exception e) {
            Log.e("error", e.toString());
        }
        return citiesList;
    }

    private  String readFromAsset(final Activity act, final String fileName)
    {
        String text = "";
        try {
            InputStream is = act.getAssets().open(fileName);

            int size = is.available();

            // Read the entire asset into a local byte buffer.
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            text = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }
}

