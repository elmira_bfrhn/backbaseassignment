package com.farahani.elmira.backbaseassignment.Objects;

/**
 * Created by Elmira on 12/20/2017.
 */

public class Coord {
    String lon;
    String lat;

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLat() {
        return lat;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLon() {
        return lon;
    }
}
