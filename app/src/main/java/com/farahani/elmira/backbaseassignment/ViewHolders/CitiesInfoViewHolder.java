package com.farahani.elmira.backbaseassignment.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.farahani.elmira.backbaseassignment.R;

/**
 * Created by Elmira on 12/20/2017.
 */

public class CitiesInfoViewHolder extends RecyclerView.ViewHolder{
    public TextView tvName;
    public TextView tvCountry;
    public CitiesInfoViewHolder(View view) {
        super(view);

        tvName = (TextView)view.findViewById(R.id.tvName);
        tvCountry= (TextView)view.findViewById(R.id.tvCountry);
    }
}