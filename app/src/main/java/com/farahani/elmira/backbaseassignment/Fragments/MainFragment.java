package com.farahani.elmira.backbaseassignment.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.farahani.elmira.backbaseassignment.Adapters.CitiesAdapter;
import com.farahani.elmira.backbaseassignment.Objects.CityInfo;
import com.farahani.elmira.backbaseassignment.R;
import com.farahani.elmira.backbaseassignment.Utils.PaginationScrollListener;
import com.farahani.elmira.backbaseassignment.Utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Elmira on 12/20/2017.
 */

public class MainFragment extends Fragment {
    ArrayList<CityInfo> cityInfoArrayList;
    RecyclerView citiesInfoRcl;
    CitiesAdapter citiesAdapter;
    LinearLayoutManager linearLayoutManager;
    RecyclerView rv;
    ProgressBar progressBar;
    //First page index
    private static final int PAGE_START = 0;
    private boolean isPagingProgressLoading = false;
    // IF current page is last page
    private boolean isLastPage = false;
    private int totalPages = 3;
    private int currentPage = PAGE_START;
    int firstItemIndexInPage = 0;
    int lastItemIndexInPage = 10;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container,
                false);

        progressBar = rootView.findViewById(R.id.progressbar);
        citiesInfoRcl = rootView.findViewById(R.id.citiesInfoRcl);

        cityInfoArrayList = new ArrayList<>();

        Bundle bundle = this.getArguments();
        cityInfoArrayList = (ArrayList<CityInfo>) bundle.getSerializable("list");

        totalPages = cityInfoArrayList.size() / 10;

        //Sort arrayList using collections
        Collections.sort(cityInfoArrayList, new Comparator<CityInfo>() {
            @Override
            public int compare(CityInfo lhs, CityInfo rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });

         InitializeAdapter(cityInfoArrayList);

        return rootView;
    }

    private void InitializeAdapter(final ArrayList<CityInfo> cityInfoArrayList) {
        Log.d("InitializeAdapter", cityInfoArrayList.size() + "");

        citiesAdapter = new CitiesAdapter(getActivity(), cityInfoArrayList);
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        citiesInfoRcl.setLayoutManager(linearLayoutManager);
        citiesInfoRcl.setItemAnimator(new DefaultItemAnimator());
        citiesInfoRcl.setAdapter(citiesAdapter);
        citiesInfoRcl.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        MapFragment mapFragment = new MapFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("name", citiesAdapter.cityInfoArrayList.get(position).getName());
                        bundle.putString("country", citiesAdapter.cityInfoArrayList.get(position).getCountry());
                        bundle.putDouble("lon", Double.valueOf(citiesAdapter.cityInfoArrayList.get(position).getCoord().getLon()));
                        bundle.putDouble("lat", Double.valueOf(citiesAdapter.cityInfoArrayList.get(position).getCoord().getLat()));
                        mapFragment.setArguments(bundle);
                        (getActivity()).getSupportFragmentManager().beginTransaction()
                                .add(R.id.frlFragmentsContainer, mapFragment).addToBackStack("mapFragment").commit();

                    }
                }));
        citiesInfoRcl.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isPagingProgressLoading = true;
                //Increment page indexfor loading next one
                currentPage += 1;
                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return (totalPages);
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isPagingProgressLoading;
            }
        });

        loadFirstPage();
    }


    public void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                citiesAdapter.getFilter().filter(newText);
                Log.d("newtext", newText);
                return true;

            }

        });
    }

    private void loadFirstPage() {
        progressBar.setVisibility(View.GONE);
        ArrayList<CityInfo> tempList = new ArrayList<>(cityInfoArrayList.subList(firstItemIndexInPage, lastItemIndexInPage));
        citiesAdapter.addAll(tempList);

        if (currentPage <= totalPages) citiesAdapter.addLoadingFooter();
        else isLastPage = true;
    }

    private void loadNextPage() {
        firstItemIndexInPage = lastItemIndexInPage + 1;
        lastItemIndexInPage = lastItemIndexInPage + 10;
        ArrayList<CityInfo> list = new ArrayList<>(cityInfoArrayList.subList(firstItemIndexInPage, lastItemIndexInPage));  // 1
        citiesAdapter.removeLoadingFooter();
        isPagingProgressLoading = false;
        citiesAdapter.addAll(list);

        if (currentPage != totalPages) citiesAdapter.addLoadingFooter();  // 5
        else isLastPage = true;
    }
}
