package com.farahani.elmira.backbaseassignment.Async;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.farahani.elmira.backbaseassignment.Interfaces.OnAsyncRequestComplete;
import com.farahani.elmira.backbaseassignment.Objects.CityInfo;
import com.farahani.elmira.backbaseassignment.Objects.Coord;
import com.farahani.elmira.backbaseassignment.Utils.GsonStreamReader;

import java.util.ArrayList;

public class JsonReadingAsyncRequest extends AsyncTask<String, String, ArrayList<CityInfo>> {

    private OnAsyncRequestComplete caller;
    ProgressDialog pDialog = null;
    Context context;
    Activity activity;

    public JsonReadingAsyncRequest(Activity a) {
        caller = (OnAsyncRequestComplete) a;
        context = a;
        this.activity = a;

    }

    @Override
    protected void onPreExecute() {
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Please wait...");
        pDialog.show();

    }

    @Override
    protected ArrayList<CityInfo> doInBackground(String... urls) {

        return ReadAndConvertJson();

    }

    @Override
    protected void onPostExecute(ArrayList<CityInfo> result) {

        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }

        caller.asyncResponse(result);

    }

    protected void onCancelled(ArrayList<CityInfo> response) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
        caller.asyncResponse(response);

    }

    private ArrayList<CityInfo> ReadAndConvertJson() {
        ArrayList<CityInfo> cityInfoArrayList = new ArrayList<>();
        CityInfo cityInfoObj;
        Coord coord;

        GsonStreamReader gsonStreamReader = new GsonStreamReader(activity);
        for (CityInfo cityInfo : gsonStreamReader.getCities()
                ) {
            Log.d("cityInfo", cityInfo.getCoord().getLat());
            cityInfoObj = new CityInfo();
            cityInfoObj.set_id(cityInfo.get_id());
            cityInfoObj.setName(cityInfo.getName());
            cityInfoObj.setCountry(cityInfo.getCountry());
            coord = new Coord();
            coord.setLat(cityInfo.getCoord().getLat());
            coord.setLon(cityInfo.getCoord().getLon());
            cityInfoObj.setCoord(coord);
            cityInfoArrayList.add(cityInfoObj);
        }
        return cityInfoArrayList;
    }


}
