package com.farahani.elmira.backbaseassignment.Objects;

/**
 * Created by Elmira on 12/20/2017.
 */

public class CityInfo {
    String name;
    String country;
    String _id;
    Coord coord;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_id() {
        return _id;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public Coord getCoord() {
        return coord;
    }
}
