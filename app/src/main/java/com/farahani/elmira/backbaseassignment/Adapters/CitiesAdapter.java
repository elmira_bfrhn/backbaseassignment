package com.farahani.elmira.backbaseassignment.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.farahani.elmira.backbaseassignment.Objects.CityInfo;
import com.farahani.elmira.backbaseassignment.R;
import com.farahani.elmira.backbaseassignment.ViewHolders.CitiesInfoViewHolder;
import com.farahani.elmira.backbaseassignment.ViewHolders.ProgressViewHolder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Elmira on 12/20/2017.
 */

public class CitiesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    Context context;
    public ArrayList<CityInfo> cityInfoArrayList;
    private ArrayList<CityInfo> filteredArrayList;

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;

    public CitiesAdapter(Context context,ArrayList<CityInfo> cityInfoArrayList) {
        this.context = context;
        filteredArrayList =new ArrayList<>();
        this.cityInfoArrayList =cityInfoArrayList;
    }

    public ArrayList<CityInfo> getCities() {
        return filteredArrayList;
    }

    public void setCities(ArrayList<CityInfo> list) {
        this.filteredArrayList = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                return viewHolder;
            case LOADING:
                View v2 = inflater.inflate(R.layout.progress_row, parent, false);
                viewHolder = new ProgressViewHolder(v2);
                return viewHolder;
        }
        return null;

    }

    @NonNull
    private CitiesInfoViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        CitiesInfoViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.card_row, parent, false);
        viewHolder = new CitiesInfoViewHolder(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CityInfo cityInfo = filteredArrayList.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                CitiesInfoViewHolder citiesInfoViewHolder = (CitiesInfoViewHolder) holder;

                citiesInfoViewHolder.tvName.setText(filteredArrayList.get(position).getName());
                citiesInfoViewHolder.tvCountry.setText(filteredArrayList.get(position).getCountry());
                break;
            case LOADING:
//                Do nothing
                break;
        }

    }

    @Override
    public int getItemCount() {
        return filteredArrayList == null ? 0 : filteredArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == filteredArrayList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    filteredArrayList = cityInfoArrayList;
                } else {

                    ArrayList<CityInfo> filteredList = new ArrayList<>();

                    for (CityInfo cityInfo : cityInfoArrayList) {

                        if (cityInfo.getName().toLowerCase().startsWith(charString)) {

                            filteredList.add(cityInfo);
                        }
                    }

                    filteredArrayList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredArrayList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredArrayList = (ArrayList<CityInfo>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    //base methods for paging

    public void add(CityInfo mc) {
        filteredArrayList.add(mc);
        notifyItemInserted(filteredArrayList.size() - 1);
    }

    public void addAll(ArrayList<CityInfo> mcList) {
        for (CityInfo mc : mcList) {
            add(mc);
        }
    }

    public void remove(CityInfo city) {
        int position = filteredArrayList.indexOf(city);
        if (position > -1) {
            filteredArrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new CityInfo());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = filteredArrayList.size() - 1;
        CityInfo item = getItem(position);

        if (item != null) {
            filteredArrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public CityInfo getItem(int position) {
        return filteredArrayList.get(position);
    }


}