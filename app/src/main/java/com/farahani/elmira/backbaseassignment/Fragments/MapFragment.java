package com.farahani.elmira.backbaseassignment.Fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.widget.TextView;

import com.farahani.elmira.backbaseassignment.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Elmira on 12/20/2017.
 */

public class MapFragment extends Fragment implements OnMapReadyCallback {
    double lat = 0;
    double lon = 0;
    String name;
    String country;
    BitmapDescriptor icon;
    MarkerOptions markerObject;
    private String title;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        GetArguments();
        SupportMapFragment fragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        fragment.getMapAsync(MapFragment.this);

        return rootView;
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setRotateGesturesEnabled(true);
        map.getUiSettings().setZoomGesturesEnabled(true);

        markerObject = new MarkerOptions();
        icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE);

        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {

            @Override
            public void onMapLoaded() {

                try {
                    map.clear();
                    LatLng latlngObject = new LatLng(lat, lon);
                    CircleOptions circle = new CircleOptions();
                    circle.center(latlngObject);
                    circle.radius(30);
                    circle.strokeColor(Color.DKGRAY);
                    markerObject.position(latlngObject);
                    CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latlngObject, 14);

                    map.animateCamera(update);
                    markerObject.icon(icon);
                    Marker marker = map.addMarker(markerObject);
                    map.addCircle(circle);
                    dropPinEffect(marker);

                } catch (Exception e) {
                }
            }
        });

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {

                try {
                    if (marker.isInfoWindowShown()) {
                        marker.hideInfoWindow();
                    } else {
                        map.setInfoWindowAdapter(new CustomInfoWindowAdapter(name));
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return false;
            }
        });
    }
    private void dropPinEffect(final Marker marker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final long duration = 3000;
        final android.view.animation.Interpolator interpolator =
                new BounceInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = Math.max(
                        1 - interpolator.getInterpolation((float) elapsed
                                / duration), 0);
                marker.setAnchor(0.5f, 1.0f + 10 * t);

                if (t > 0.0) {
                    handler.postDelayed(this, 15);
                } else {
                    marker.showInfoWindow();
                }
            }
        });
    }

    private class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        String location_str;
        private View view;

        public CustomInfoWindowAdapter(String location_str) {
            view = getActivity().getLayoutInflater().inflate(R.layout.custom_info_window,
                    null);
            this.location_str = name;
        }

        @Override
        public View getInfoContents(Marker marker) {
            if (marker != null
                    && marker.isInfoWindowShown()) {
                marker.hideInfoWindow();
                marker.showInfoWindow();
            }
            return null;
        }

        @Override
        public View getInfoWindow(final Marker marker) {
            TextView tvCountry = ((TextView) view.findViewById(R.id.tvCountry));
            tvCountry.setText(country);

            TextView tvName = ((TextView) view.findViewById(R.id.tvName));
            tvName.setText(name);
            return view;
        }
    }

    private void GetArguments() {

        Bundle bundle = this.getArguments();
        lat = bundle.getDouble("lat");
        lon = bundle.getDouble("lon");
        name = bundle.getString("name");
        country = bundle.getString("country");

    }

}

